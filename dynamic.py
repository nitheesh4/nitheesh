import pandas as pd


def load(data):
    year = data["Oscar Year"].str.split("/", n=1, expand=True)
    year1 = year.dropna()[1].tolist()
    year2 = data["Oscar Year"].apply(lambda x: x if (len(x) == 4) else None)
    year2 = year2.dropna().tolist()
    for i in range(len(year1)):
        year1[i] = "19" + year1[i]
    data["year"] = year1 + year2
    data["year"] = data["year"].astype("int")
    return data


def preprocess(data):
    data = load(data)
    year = data["year"].unique()
    s = "decade"
    date = [str(year[i]) + "-" + str(year[i + 9]) for i in range(0, len(year), 10)]
    decades = [s + str(i) for i in range(1, int(len(year) / 10) + 1)]
    dataset = pd.DataFrame({"decades": decades, "year": date})
    return dataset


#  Description of the decades
def years():
    data = pd.read_csv('oscars_df.csv')
    return preprocess(data)


def dropdown(data, handler):
    data = load(data)
    return preprocess(data)


def output(data, handler):
    awards = ["Winner"]
    year = handler.get_argument('year_', '2011-2020')
    year = year.split("-")
    start = int(year[0])
    last = int(year[1])
    if len(handler.args) >= 1:
        awards = handler.args['award']
    data = load(data)
    if (len(awards) == 1):
        data = data.loc[data["Award"] == awards[0]]
    dataset = pd.DataFrame({"Films": data["Film"],
                            "award": data["Award"],
                            "year": data["year"],
                            "Rating": data["IMDB Rating "],
                            "Votes": data["IMDB Votes"]})
    dataset = dataset.loc[(dataset["year"] >= start) & (dataset["year"] <= last)]
    return dataset


def bar_graph(data, handler):
    dataset = output(data, handler)
    awards = ["Winner"]
    year = handler.get_argument('year_', '2011-2020')
    year = year.split("-")
    if len(handler.args) > 1:
        awards = handler.args["award"]
    if (len(awards) == 2) or (awards[0] == "Winner"):
        return dataset.loc[(dataset["award"] == "Winner")]
    final_dataset = (dataset[(dataset["year"] == int(year[0]))])
    final_dataset = final_dataset[(final_dataset["Rating"] == final_dataset["Rating"].max())]
    for i in range(int(year[0]) + 1, int(year[1]) + 1):
        d = (dataset[(dataset["year"] == i)])
        d = d[(d["Rating"] == d["Rating"].max())]
        if d.shape[0] > 1:
            d = d[(d["Votes"] == d["Votes"].max())]
        final_dataset = final_dataset.merge(d, how='outer')
    return final_dataset
